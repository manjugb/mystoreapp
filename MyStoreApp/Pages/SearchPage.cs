﻿using MyStoreApp.Base;
using MyStoreApp.Helper;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Internal;
using OpenQA.Selenium.Support.UI;
//using OpenQA.Selenium.Support.PageObjects;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStoreApp.Pages
{
    public sealed class SearchPage
    
    {

        private IWebDriver driver;
    

    public SearchPage(IWebDriver _driver)
    {
        driver = _driver;
        PageFactory.InitElements(driver, this);
        GlobalDefinitions.ExcelLib.PopulateInCollection(paths.ExcelPath, "ProductSearch");
        searchkey = GlobalDefinitions.ExcelLib.ReadData(2, "SearchKey");

    }


    private string searchkey;
    #region  Initialize Web Elements 
    [FindsBy(How = How.CssSelector, Using = "#search_query_top")]
    private IWebElement SearchForProduct { get; set; }


    [FindsBy(How = How.XPath, Using = "//form[@id='searchbox']//button[@type='submit']")]
    private IWebElement SearchButton { get; set; }

    [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[2]/h1[1]/span[2]")]
    private IWebElement SearchResults { get; set; }


    #endregion




    internal void EnterSearchKey(string searchkey)
    {
        Base.GlobalDefinitions.wait(30);
        if (SearchForProduct.Displayed)
        {
            GlobalDefinitions.wait(30);
            SearchForProduct.SendKeys(searchkey);
        }
        else
        {
            Console.WriteLine("Enter Search Key", SearchForProduct);
        }
    }



    internal void ClickOnSubmit()
    {
        if (SearchButton.Displayed)
        {
            Base.GlobalDefinitions.wait(30);
            SearchButton.Click();
        }
        else
        {
            Console.WriteLine("Search Button", SearchButton);
        }
    }

    internal void VerifyProductResultsDisplayed(string Results)
    {
        if (SearchResults.Displayed)
        {
            GlobalDefinitions.wait(30);
            string actResults = SearchResults.Text;
            Assert.AreEqual(actResults, Results);

        }
        else
        {
            string actResults = SearchResults.Text;
            Assert.AreNotEqual(actResults, Results);
        }
    }


}
}
