﻿using System;
using MyStoreApp.Base;
using MyStoreApp.Helper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace MyStoreApp.Pages
{
    public class SignIn
    {

        public SignIn(IWebDriver driver)
        {

            this.driver = driver;
            //Populate the excel data
            GlobalDefinitions.ExcelLib.PopulateInCollection(paths.ExcelPath, "SignIn");
            username = GlobalDefinitions.ExcelLib.ReadData(2, "Username");
            password = GlobalDefinitions.ExcelLib.ReadData(2, "Password");

            PageFactory.InitElements(driver, this);
        }


        private string username;
        private string password;

        private readonly IWebDriver driver;

        #region  Initialize Web Elements 


        //Sign In Link
        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[1]/header[1]/div[2]/div[1]/div[1]/nav[1]/div[1]/a[1]")]
        private IWebElement SignIntab { get; set; }

        // Finding the Email Field
        [FindsBy(How = How.CssSelector, Using = "#email")]
        private IWebElement Email { get; set; }

        //Finding the Password Field
        [FindsBy(How = How.CssSelector, Using = "#passwd")]
        private IWebElement Password { get; set; }

        //Finding the Login Button
        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[2]/form[1]/div[1]/p[2]/button[1]/span[1]")]
        private IWebElement LoginBtn { get; set; }




        //Finding the Login Button
        [FindsBy(How = How.LinkText, Using = "Sign out")]
        private IWebElement LogOutBtn { get; set; }






        //span[contains(text(),'Log out')]

        #endregion

        internal void LoginSteps()
        {
            SignIntab.Click();

            //enter email
            Email.SendKeys(username);
            //enter password
            Password.SendKeys(password);
            //click login button
            LoginBtn.Click();

        }
    }
}



