﻿using BoDi;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStoreApp.Base
{
    public class WebDriverContext
    {
        private readonly IObjectContainer _objectContainer;
        public IWebDriver driver { get; set; }

        public WebDriverContext(IObjectContainer objectContainer)
        {
            _objectContainer = objectContainer;

        }
    }
}
