﻿@MyStore
Feature: Search Product
@SearchProduct
Scenario: MyStore Page As a User Able to search product
Given I am on MyStore page
Then I Enter "<searchkey>"
Then I Click on Search Button
Then I Verify "<results>"
Examples:
|searchkey|results|
|Printed|5 results have been found.|
|Chiffon Dress|2 results have been found.|
|Dress|7 results have been found.|
|Chiffon Dress|2 results have been found.|