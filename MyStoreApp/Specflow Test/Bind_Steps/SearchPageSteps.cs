﻿using MyStoreApp.Config;
using MyStoreApp.Base;
using MyStoreApp.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using AventStack.ExtentReports;
using RazorEngine.Compilation.ImpromptuInterface;
using MyStoreApp.Pages;

namespace MyStoreApp.Specflow_Test.Bind_Steps
{
    [Binding]
    public sealed class SearchPageSteps
    {
        //create profile page object


       
        private readonly IWebDriver driver;

        internal SearchPage HomePage { get; private set; }

        public SearchPageSteps(IWebDriver driver)
        {
            this.driver = driver;
            
        }

        [Given(@"I am on MyStore page")]
        public void GivenIAmOnLPage()
        {
            HomePage = new SearchPage(driver);
            
        }

        [Then(@"I Enter ""(.*)""")]
        public void ThenIEnterSearchkey(string searchkey)
        {
            HomePage.EnterSearchKey(searchkey);
        }

        [Then(@"I Verify ""(.*)""")]
        public void ThenIverifyProductResults(string prodresults)
        {
            HomePage.VerifyProductResultsDisplayed(prodresults);
        }

        [Then(@"I Click on Search Button")]
        public void ThenIClickOnAvoniteSpage()
        {
            HomePage.ClickOnSubmit();
        }


        [Then(@"I Click on Close Browser")]
        public void ThenICloseBrowser()
        {
            driver.Quit();

        }


    }
}